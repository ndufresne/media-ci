#!/bin/sh
set -e

VALID_ARGS="--kernel_dir --cross_compile --cc --arch --abi_reference"

CI_DIR=$(dirname "$(realpath "$0")")
. "$CI_DIR"/common-init.sh

ABI_REFERENCE=$(realpath "$ABI_REFERENCE")
cd "$KERNEL_DIR" || exit 1

cat >test.c <<'EOF'
#include <linux/time.h>
#include <linux/string.h>
typedef unsigned long           uintptr_t;
#include <stdbool.h>
#include <stddef.h>
#include <linux/videodev2.h>
#include <linux/v4l2-subdev.h>
#include <linux/media.h>
/*#include <linux/cec.h>*/

union {
EOF
cat include/uapi/linux/videodev2.h include/uapi/linux/v4l2-subdev.h include/uapi/linux/media.h | perl -ne 'if (m/\#define\s+([A-Z][A-Z0-9_]+)\s+\_IO.+\(.*\,\s*([^\)]+)\)/) {print "$2\n"}' | LC_COLLATE=C sort | uniq | perl -ne 'chomp; print "$_ t$num;\n"; $num++;' >>test.c
echo '} x;' >>test.c

ln -s asm-generic include/uapi/asm
ln -s asm-generic include/asm
"$CROSS_COMPILE$CC" -g -Og -c test.c -D__KERNEL__ -I include/uapi -I include

abi-dumper -quiet test.o
abi-compliance-checker -l test.o -old "$ABI_REFERENCE" -new ABI.dump

. "$CI_DIR"/common-end.sh
