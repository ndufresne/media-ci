stages:
  - rebase
  - lint
  - build
  - bisect
  - virtme
  - trust

.ci-run-policy:
  # Retry jobs after runner system failures
  retry:
    max: 2
    when:
      - runner_system_failure
  # Cancel CI run if a newer commit is pushed to the same branch
  interruptible: true

.non-schedules:
  extends:
    - .ci-run-policy
  except:
    - schedules

.patch_tests:
  extends:
    - .ci-run-policy
  before_script:
    - git fetch --depth=1 origin main
  variables:
    GIT_DEPTH: 50
  except:
    - main
    - schedules

checkpatch:
  extends:
    - .patch_tests
  stage: lint
  image: registry.freedesktop.org/linux-media/media-ci/checkpatch
  script:
    - sh /media-ci/testgroup-checkpatch.sh

abi:
  extends: .non-schedules
  stage: lint
  image: registry.freedesktop.org/linux-media/media-ci/abi
  script:
    - sh /media-ci/testgroup-abi.sh

build:
  extends: .non-schedules
  stage: build
  image: registry.freedesktop.org/linux-media/media-ci/build
  script:
    - sh /media-ci/testgroup-build.sh

build-gcc11:
  extends: build
  image: registry.freedesktop.org/linux-media/media-ci/build-gcc11

build-cross:
  extends: build
  image: registry.freedesktop.org/linux-media/media-ci/build-cross

build-llvm:
  extends: build
  image: registry.freedesktop.org/linux-media/media-ci/build-llvm
  script:
    - sh /media-ci/testgroup-llvm.sh

doc:
  extends: .non-schedules
  stage: build
  image: registry.freedesktop.org/linux-media/media-ci/doc
  script:
    - sh /media-ci/testgroup-doc.sh

static-analyzers:
  extends: .non-schedules
  stage: build
  image: registry.freedesktop.org/linux-media/media-ci/static
  script:
    - sh /media-ci/testgroup-static.sh
  allow_failure: true

bisect:
  extends:
    - .patch_tests
  stage: bisect
  image: registry.freedesktop.org/linux-media/media-ci/bisect
  script:
    - sh /media-ci/testgroup-bisect.sh

virtme64:
  extends: .non-schedules
  stage: virtme
  image: registry.freedesktop.org/linux-media/media-ci/virtme
  script:
    - sh /media-ci/testgroup-virtme.sh
  allow_failure: true

virtme32:
  extends: .non-schedules
  stage: virtme
  image: registry.freedesktop.org/linux-media/media-ci/virtme
  script:
    - sh /media-ci/testgroup-virtme.sh --virtme_args "/media-ci/third_party/v4l-utils -32"
  allow_failure: true

trust:
  extends:
    - .patch_tests
  stage: trust
  image: registry.freedesktop.org/linux-media/media-ci/trust
  script:
    - sh /media-ci/testgroup-trust.sh

rebase:
  stage: rebase
  extends: .ci-run-policy
  image: registry.freedesktop.org/linux-media/media-ci/rebase
  script:
    - git config --global user.email "automatic-rebaser@linuxtv.org"
    - git config --global user.name "Automatic Rebaser"
    - gitlab_url="https://gitlab-ci-token:${deploy_key}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}.git"
    - git push "$gitlab_url" "HEAD:refs/heads/autorebaser-backup/$(date -I)"
    - linus_url="git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git"
    - git fetch --shallow-since=format:relative:"1 month" "$linus_url" master
    - new_target=$(git rev-parse FETCH_HEAD)
    - git fetch --shallow-since=format:relative:"1 month" "$gitlab_url" main
    - git rebase "$new_target"
    - git push -f "$gitlab_url" HEAD:main
  only:
    - schedules
