#!/bin/sh

if [ "$SKIP_CLEAN" != 1 ]; then
  sh "$CI_DIR"/kernel-clean.sh
fi
echo \""$CI_COMMAND"\" Completed!
