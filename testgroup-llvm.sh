#!/bin/sh
set -e

CI_DIR=$(dirname "$(realpath "$0")")

VALID_ARGS="--kernel_dir"
. "$CI_DIR"/common-init.sh
cd "$KERNEL_DIR"

set -v
sh "$CI_DIR"/test-build.sh --arch x86_64 --llvm 1
sh "$CI_DIR"/test-build.sh --arch x86 --llvm 1
sh "$CI_DIR"/test-build.sh --arch arm64 --llvm 1
# TODO LLVM keeps complaining about alignment errors on arm. Let's investigate
# if it is real or not. Until then, ignore the error
sh "$CI_DIR"/test-build.sh --arch arm --llvm 1 --ignore_warnings 1
sh "$CI_DIR"/test-build.sh --arch powerpc --llvm 1
