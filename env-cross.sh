#!/bin/sh

for arch in x86_64-linux-gnu i686-linux-gnu aarch64-linux-gnu arm-linux-gnueabi powerpc64-linux-gnu; do
  PATH="$PWD/cross/bin/$arch/bin:$PATH"
done

export PATH
