#!/bin/sh
set -e

VALID_ARGS="--kernel_dir --cross_compile --cross_compile32 --cc --arch"

CI_DIR=$(dirname "$(realpath "$0")")
. "$CI_DIR"/common-init.sh

cd "$KERNEL_DIR" || exit 1

headers_dir=out/usr
make -j INSTALL_HDR_PATH=$headers_dir headers_install >/dev/null

"$CROSS_COMPILE$CC" "$CI_DIR"/pahole/pahole.c -c -g -o pahole64.o -I $headers_dir/include
"$CROSS_COMPILE32$CC" "$CI_DIR"/pahole/pahole.c -c -g -o pahole32.o -I $headers_dir/include

pahole pahole64.o > pahole64.pa
pahole pahole32.o > pahole32.pa

if ! cmp pahole64.pa pahole32.pa ; then
  echo 32 vs 64 bit differences
  exit 1
fi

for pafile in pahole32.pa pahole64.pa; do
  if grep -E 'hole|padding:' "$pafile"; then
    echo holes or padding in "$pafile"
    exit 1
  fi
done

. "$CI_DIR"/common-end.sh
