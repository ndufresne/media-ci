#!/bin/sh
set -e

docker_tag_push()
{
  image=$1
  tag=$2
  docker tag "$image" "$image":"$tag"
  if [ "$do_skip_push" != 1 ]; then
    docker push "$image":"$tag"
  fi
}

usage()
{
  echo "Usage build-dockers.sh --release --skip_push"
}

CI_REGISTRY_IMAGE="${CI_REGISTRY_IMAGE:=registry.freedesktop.org/linux-media/media-ci}"
CI_COMMIT_SHA="${CI_COMMIT_SHA:=$(git rev-parse HEAD)}"

while [ $# != 0 ]; do
  case $1 in
    --release) do_release=1;;
    --skip_push) do_skip_push=1;;
    --help) usage && exit 1;;
  esac
  shift;
done

for dockerfile in docker/*/Dockerfile; do
  name=$(basename "$(dirname "$dockerfile")")
  image="$CI_REGISTRY_IMAGE/$name"
  docker build -t "$image" -f "$dockerfile" .
  docker_tag_push "$image" "$CI_COMMIT_SHA"
  if [ "$do_release" = 1 ]; then
    docker_tag_push "$image" latest
    docker_tag_push "$image" "$(date -I)"
  fi
done
