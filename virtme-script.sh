#!/bin/sh
set -ev

util_path=$1
args=$2

PATH="$util_path"/build/usr/bin:$PATH

cd "$util_path"/contrib/test/
# shellcheck disable=SC2086
./test-media mc -kmemleak $args

echo b >/proc/sysrq-trigger
