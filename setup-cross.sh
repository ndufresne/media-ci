#!/bin/sh
set -e

sh cross.sh setup
sh cross.sh all
sh cross.sh build-clean
