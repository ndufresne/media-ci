#!/bin/sh
set -e

CI_DIR=$(dirname "$(realpath "$0")")

VALID_ARGS="--kernel_dir"
. "$CI_DIR"/common-init.sh
cd "$KERNEL_DIR"

set -v
sh "$CI_DIR"/test-spec.sh
sh "$CI_DIR"/test-kernel-doc.sh
