#!/bin/sh
set -e

VALID_ARGS="--kernel_dir --git_origin"

CI_DIR=$(dirname "$(realpath "$0")")
. "$CI_DIR"/common-init.sh

cd "$KERNEL_DIR" || exit 1

git rebase "$GIT_ORIGIN" --exec "sh $CI_DIR/test-build.sh --ignore_warnings 1 --skip_clean 1 && sh $CI_DIR/test-misc.sh --skip_clean 1"

. "$CI_DIR"/common-end.sh
