#!/bin/sh
set -e

VALID_ARGS="--kernel_dir --kernel_config --arch --cross_compile --llvm --ignore_warnings --skip_clean"

CI_DIR=$(dirname "$(realpath "$0")")
. "$CI_DIR"/common-init.sh

if [ -r "$KERNEL_CONFIG" ]; then
  KERNEL_CONFIG=$(realpath "$KERNEL_CONFIG")
fi

cd "$KERNEL_DIR"

[ -z "$LLVM" ] && KCFLAGS="-Wmaybe-uninitialized"

sh "$CI_DIR"/kernel-config.sh
make W=1 KCFLAGS="$KCFLAGS" -j drivers/staging/media/ drivers/media/ drivers/input/touchscreen/ 3>&1 1>&2 2>&3 | tee build.log
grep -E -i "(error:|^make.*Error )" build.log && exit 1

if [ "$IGNORE_WARNINGS" != 0 ]; then
  . "$CI_DIR"/common-end.sh
  exit 0
fi

grep -v "^scripts/genksyms/parse.y:" build.log | grep -v "^./arch/powerpc/include" | grep -v "^drivers/input/touchscreen" | grep -v "\[-Wconflicts-[sr][sr]\]" | grep -v "^arch/arm64/kernel/vdso/vgettimeofday.c"  | grep -i -E "(error:|warning:|warn:|Error 1)" && exit 1

. "$CI_DIR"/common-end.sh
