#!/bin/sh
set -e

VALID_ARGS="--kernel_dir"

CI_DIR=$(dirname "$(realpath "$0")")
. "$CI_DIR"/common-init.sh

cd "$KERNEL_DIR" || exit 1

sed s,userspace-api/media,media/userspace-api,g -i Documentation/Makefile
mkdir -p Documentation/media
cat <<EOF >Documentation/media/index.rst
.. SPDX-License-Identifier: GPL-2.0

.. include:: <isonum.txt>

**Copyright** |copy| 1991-: LinuxTV Developers

================================
Linux Kernel Media Documentation
================================

.. toctree::
	:maxdepth: 4

        userspace-api/index
        driver-api/index
        admin-guide/index
EOF

ln -s ../admin-guide/media Documentation/media/admin-guide
ln -s ../driver-api/media Documentation/media/driver-api
ln -s ../userspace-api/media Documentation/media/userspace-api

make SPHINXDIRS="media" htmldocs 2>&1 | tee spec.log
grep -E "(ERROR:|WARNING:)" spec.log && exit 1
. "$CI_DIR"/common-end.sh
