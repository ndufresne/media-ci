#!/bin/sh
set -e

VALID_ARGS="--kernel_dir --git_origin --patches_max --uploaders"

CI_DIR=$(dirname "$(realpath "$0")")
. "$CI_DIR"/common-init.sh
. "$CI_DIR"/kernel-helper.sh

cd "$KERNEL_DIR" || exit 1
check_patchset_size

check() {
  file="$1"
  list=
  mkfifo grepout
  grep "^\(Signed-off\|Reviewed\)-by:" "$file" > grepout &
  while read -r tagline; do
    echo "  tag: $tagline"
    issuer=$(echo "$tagline" | cut -d ":" -f 2)
    canon_name=$(git check-mailmap "$issuer")
    list="$list $canon_name"
  done < grepout
  rm grepout

  trust_count=0
  if [ -n "$list" ]; then
    while read -r trusted; do
      if echo "$list" | grep -q "$trusted"; then
        trust_count=$((trust_count + 1))
      fi
    done < "$UPLOADERS"
  fi

  if [ "$trust_count" -lt 2 ]; then
    echo "WARNING: Missing two Signed-off-by or Reviewed-by from uploaders"
    return 1
  else
    echo "OK"
    return 0
  fi
}

errno=0
git format-patch "$GIT_ORIGIN" > /dev/null
for patch in *.patch; do
  echo "checking $patch"
  check "$patch"
  [ $? = 1 ] && errno=1
done

[ $errno != 0 ] && exit $errno

. "$CI_DIR"/common-end.sh

