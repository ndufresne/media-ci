#!/bin/sh
set -e

CI_DIR=$(dirname "$(realpath "$0")")

VALID_ARGS="--kernel_dir"
. "$CI_DIR"/common-init.sh
cd "$KERNEL_DIR"

set -v
sh "$CI_DIR"/test-build.sh --arch x86_64 --cross_compile x86_64-linux-gnu-
sh "$CI_DIR"/test-build.sh --arch x86 --cross_compile i686-linux-gnu-
sh "$CI_DIR"/test-build.sh --arch arm64 --cross_compile aarch64-linux-gnu- --kernel_config "$CI_DIR"/configs/arm64.config
sh "$CI_DIR"/test-build.sh --arch arm --cross_compile arm-linux-gnueabi- --kernel_config "$CI_DIR"/configs/arm.config
sh "$CI_DIR"/test-build.sh --arch powerpc --cross_compile powerpc64-linux-gnu- --kernel_config "$CI_DIR"/configs/powerpc64.config

sh "$CI_DIR"/test-build.sh --arch x86_64 --cross_compile x86_64-linux-gnu- --kernel_config "$CI_DIR"/configs/no-acpi.config
sh "$CI_DIR"/test-build.sh --arch x86_64 --cross_compile x86_64-linux-gnu- --kernel_config "$CI_DIR"/configs/no-debug-fs.config
sh "$CI_DIR"/test-build.sh --arch x86_64 --cross_compile x86_64-linux-gnu- --kernel_config "$CI_DIR"/configs/no-of.config
sh "$CI_DIR"/test-build.sh --arch x86_64 --cross_compile x86_64-linux-gnu- --kernel_config "$CI_DIR"/configs/no-pm.config
sh "$CI_DIR"/test-build.sh --arch x86_64 --cross_compile x86_64-linux-gnu- --kernel_config "$CI_DIR"/configs/no-pm-sleep.config
