#!/bin/sh
set -e

CI_DIR=$(dirname "$(realpath "$0")")

VALID_ARGS="--kernel_dir"
. "$CI_DIR"/common-init.sh
cd "$KERNEL_DIR" || exit 1

set -v

sh "$CI_DIR"/test-compile-all.sh

sh "$CI_DIR"/test-pahole.sh --cross_compile x86_64-linux-gnu- --cross_compile32 i686-linux-gnu-
sh "$CI_DIR"/test-pahole.sh --cross_compile aarch64-linux-gnu- --cross_compile32 arm-linux-gnueabi-
sh "$CI_DIR"/test-pahole.sh --cross_compile powerpc64-linux-gnu- --cross_compile32 powerpc64-linux-gnu-

sh "$CI_DIR"/test-abi-dumper.sh --arch x86_64 --cross_compile x86_64-linux-gnu-
sh "$CI_DIR"/test-abi-dumper.sh --arch x86 --cross_compile i686-linux-gnu-
sh "$CI_DIR"/test-abi-dumper.sh --arch arm64 --cross_compile aarch64-linux-gnu-
sh "$CI_DIR"/test-abi-dumper.sh --arch arm --cross_compile arm-linux-gnueabi-
sh "$CI_DIR"/test-abi-dumper.sh --arch powerpc --cross_compile powerpc64-linux-gnu-
