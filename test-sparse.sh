#!/bin/sh
set -e

VALID_ARGS="--kernel_dir --kernel_config --arch --cross_compile --sparse"

# Override default
[ -z "$ARCH" ] && ARCH=x86

CI_DIR=$(dirname "$(realpath "$0")")
. "$CI_DIR"/common-init.sh

if [ -r "$KERNEL_CONFIG" ]; then
  KERNEL_CONFIG=$(realpath "$KERNEL_CONFIG")
fi

cd "$KERNEL_DIR" || exit 1

sh "$CI_DIR"/kernel-config.sh
make -i -j W=1 C=1 CHECK="$SPARSE" CF="-D__CHECK_ENDIAN__ -fmemcpy-max-count=11000000" KCFLAGS="-Wmaybe-uninitialized" drivers/media/ drivers/staging/media/ 3>&1 1>&2 2>&3 | tee build.log
grep -i -E "(error:|warning:|warn:)" build.log && exit 1

. "$CI_DIR"/common-end.sh
