#!/bin/sh
set -e

VALID_ARGS="--kernel_dir --git_origin --patches_max"

CI_DIR=$(dirname "$(realpath "$0")")
. "$CI_DIR"/common-init.sh
. "$CI_DIR"/kernel-helper.sh

cd "$KERNEL_DIR" || exit 1
check_patchset_size

errno=0
git format-patch "$GIT_ORIGIN"
for patch in *.patch; do
  scripts/checkpatch.pl --strict "$patch" || errno=1

  if ! grep -q "^Subject:.* media:" "$patch"; then
    echo "WARNING: $patch: Missing 'media:' prefix in Subject" && errno=1
  fi
  if grep -q "^diff .*/boot/dts/" "$patch"; then
    echo "WARNING: $patch: Device tree changes should not be part of the media subsystem" && errno=1
  fi
  if grep -q '^Cc:.*hverkuil' "$patch"; then
    echo "WARNING: $patch: Don't Cc Hans Verkuil" && errno=1
  fi
  if grep -q '^Cc:.*mchehab' "$patch"; then
    echo "WARNING: $patch: Don't Cc Mauro Carvalho Chehab" && errno=1
  fi
done

[ $errno != 0 ] && exit $errno

. "$CI_DIR"/common-end.sh
