#!/bin/sh
set -e

VALID_ARGS="--kernel_dir"

CI_DIR=$(dirname "$(realpath "$0")")
. "$CI_DIR"/common-init.sh

cd "$KERNEL_DIR" || exit 1

make ARCH=i386 allyesconfig
# shellcheck disable=SC2046
configs=$(grep -E "(^config |^menuconfig )" $(find drivers/staging/media/ -name Kconfig) $(find drivers/media/ -name Kconfig)  | cut -d' ' -f 2)

errno=0
for config in $configs; do
  case $config in
    MEDIA_HIDE_ANCILLARY_SUBDRV) continue;;
    VIDEO_TEGRA_TPG) continue;;
  esac
  state=$(./scripts/config -k -s "$config")
  if [ "$state" = n ] || [ "$state" = undef ]; then
    echo Unable to build "$config"
    errno=1
  fi
done
[ $errno = 0 ] || exit $errno

. "$CI_DIR"/common-end.sh
