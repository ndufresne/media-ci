#!/bin/sh
set -e

top="$(realpath "$PWD")"/third_party

mkdir -p "$top"
cd "$top"

git clone git://repo.or.cz/smatch.git
cd smatch
make -j HAVE_LLVM=no
cd ..

git clone git://linuxtv.org/v4l-utils.git
cd v4l-utils
meson setup -Dv4l2-compliance-32=true -Dv4l2-ctl-32=true -Dprefix="$top/v4l-utils/build/usr" -Dudevdir="$top/v4l-utils/build/usr/lib/udev" -Dsystemdsystemunitdir="$top/v4l-utils/build/usr/lib/systemd" build/
ninja -C build/
ninja -C build install
