#!/bin/sh
set -e

VALID_ARGS="--kernel_dir --skip_clean"

CI_DIR=$(dirname "$(realpath "$0")")
. "$CI_DIR"/common-init.sh

cd "$KERNEL_DIR" || exit 1

git grep -q 'str[nl]\?cpy[^_]' drivers/staging/media/ drivers/media/ && echo Found unsafe functions && exit 1

. "$CI_DIR"/common-end.sh
