#!/bin/sh
set -e

VALID_ARGS="--kernel_dir"

CI_DIR=$(dirname "$(realpath "$0")")
. "$CI_DIR"/common-init.sh

cd "$KERNEL_DIR" || exit 1

doc_headers=include/uapi/linux/lirc.h
doc_headers="$doc_headers include/uapi/linux/videodev2.h"
doc_headers="$doc_headers include/uapi/linux/ivtv*.h"
doc_headers="$doc_headers include/uapi/linux/max2175.h"
doc_headers="$doc_headers include/uapi/linux/media*.h"
doc_headers="$doc_headers include/uapi/linux/v4l2*.h"
doc_headers="$doc_headers include/uapi/linux/uvcvideo.h"
doc_headers="$doc_headers include/uapi/linux/xilinx-v4l2-controls.h"
doc_headers="$doc_headers include/uapi/linux/ccs.h"
doc_headers="$doc_headers include/uapi/linux/smiapp.h"
doc_headers="$doc_headers include/uapi/linux/cec*.h"
doc_headers="$doc_headers "$(find include/media)
doc_headers="$doc_headers "$(find include/uapi/linux/dvb)
doc_headers="$doc_headers "$(find drivers/media/|grep \\\.h$)
doc_headers="$doc_headers "$(find drivers/staging/media/|grep -v atomisp|grep \\\.h$)

out=test-doc.out
rm -f "$out"
for header in $doc_headers; do
  scripts/kernel-doc -none "$header" 2>&1 | tee -a "$out"
done

grep -q -E '(warning:)|(error:)' -i "$out" && exit 1

. "$CI_DIR"/common-end.sh
