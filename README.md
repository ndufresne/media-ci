# Linux-media CI

This is a set of scripts to test and validate the media subsystem of the Linux
Kernel. It is meant to help contributors, uploaders and maintainers of our
beloved subsystem.

[[_TOC_]]

## Preparation

The scripts can be used in four different ways:

-   Gitlab
-   Public containers
-   Self generated container
-   Locally

### Gitlab

This is the simplest way to run the tests. You just need to have git installed
on your computer and an account on the linux-media
[Gitlab group](https://gitlab.freedesktop.org/linux-media){:.external}.

Simply push your code to a branch (typically with the prefix `$username/`) under
`https://gitlab.freedesktop.org/linux-media/media-staging` and the CI system
would automatically run all the tests on your behalf. Eg:

```
user@host:~/linux$ git remote add gitlab git@gitlab.freedesktop.org:linux-media/media-staging.git
user@host:~/linux$ git push gitlab HEAD:refs/heads/user/newfeature
```

### Public container

The repository generates a
[container](https://gitlab.freedesktop.org/linux-media/media-ci/container_registry/48202){:.external}
with all the tools required to test your kernel tree. You just need to have a
container manager on your system like podman or docker to use it. This can be
installed using your [favourite distro](https://www.debian.org/){:.external}. Eg
for Debian:

```
user@host:~/linux$ apt-get install podman
```

The container has the media-ci scripts located at `/media-ci`, and all the
dependencies are already pre-installed.

The container can be instanced like this:

```
user@host:~/linux$ podman run --pids-limit=-1 --rm -it -v .:/workdir  registry.freedesktop.org/linux-media/media-ci/media-ci /media-ci/testgroup-virtme.sh --kernel_dir /workdir
```

Where:

-`v .:/workdir` Mounts the current directory (your kernel tree) in the folder
`/workdir` on the container

`/media-ci/testgroup-virtme.sh --kernel_dir /workdir` is the actual test

### Self generated container

The media-ci container can also be built locally. After cloning the `media-ci`
(this) repository to your computer run:

```
user@host:~/linux$ podman run --pids-limit=-1 --rm -it -v .:/workdir  media-ci /media-ci/testgroup-virtme.sh --kernel_dir /workdir
```

This will generate a container tagged `media-ci` on your local container
repository.

Call it like: `user@host:~/linux$ podman run --pids-limit=-1 --rm -it -v
.:/workdir media-ci /media-ci/testgroup-virtme.sh --kernel_dir /workdir`

### Locally

Clone the `media-ci` repository and execute the script `setup-third_party.sh`
inside it.

The list of dependencies can be inferred by looking at
`docker/media-ci/Dockerfile`.

Call the different `test*.sh` scripts from a clean linux repository. The scripts
will run the different tests and clean-up after them if they succeed. The exit
code of the script determines if the test has passed or failed. E.g:

```
user@host:~/linux$ sh ~/media-ci/test-pahole.sh
"/home/media-ci/test-pahole.sh " Completed!
```

## Tests

The tests are grouped in `testgroups` that can be considered the top level unit
testing.

They can be found in the repository with the name `testgroup-*.sh` they call one
or more smaller tests that are named `test-*.sh`, that can be also called
directly.

When a test fails, it will have a non-zero retcode and will print the error.

When a test succeeds it will have a 0 retcode and will revert the repository to
a clean state.

Test behaviour can be modified with environment variables and arguments. Just
run the commands with the --`help` argument to get the list of valid arguments
and env variables. E.g

```
Usage: test-pahole.sh [--help] [--kernel_dir value] [--cross_compile value] [--cross_compile32 value] [--cc value] [--arch value]

Arguments:
 --help: show this help
 --kernel_dir /home/user/linux: Path of the kernel directory
 --cross_compile x86_64-linux-gnu-: Cross-compile prefix
 --cross_compile32 i686-linux-gnu-: Cross-compile prefix for 32-bit ABI-checks comparison
 --cc gcc: Compiler
 --arch x86_64: Kernel architecture

Environment Variables:
 --kernel_dir: KERNEL_DIR
 --cross_compile: CROSS_COMPILE
 --cross_compile32: CROSS_COMPILE32
 --cc: CC
 --arch: ARCH

```

### ABI testgroup

It validates that the ABI has not changed. It contains three tests:

-   `test-compile-all.sh`:
    -   Makes sure that all the media code is compiled.
-   `test-pahole.sh`:
    -   Compiles `pahole/pahole.c` for all the supported arches.
    -   It checks that there are not holes or padding.
    -   It checks that arm32 and x86 are identical to arm64 and x86_64
        respectively.
-   `test-abi-dumper.sh`:
    -   Autogenerates a C file that contains all the media structures.
    -   Use `abi-compliance-checker` (from `abi-dumper`) to validate that there
        is no ABI breakage.

```
Usage: testgroup-abi.sh [--help] [--kernel_dir value]

Arguments:
 --help: show this help
 --kernel_dir /home/user/linux: Path of the kernel directory

Environment Variables:
 --kernel_dir: KERNEL_DIR
```

### Build testgroup

It validates that the code from the media subsystem can be built. It contains
one test:

-   `test-build.sh`:
    -   Builds the code using the provided options

The build testgroup builds for x86 and x86_64 using the allyesconfig. It also
builds using manually created config files under the `configs/` folder.

```
Usage: testgroup-build.sh [--help] [--kernel_dir value]

Arguments:
 --help: show this help
 --kernel_dir /home/user/linux: Path of the kernel directory

Environment Variables:
 --kernel_dir: KERNEL_DIR
```

### LLVM testgroup

Similar to the build testgroup but using the clang compiler instead of gcc. It
builds for arm, arm64, x86, x86_64, and powerpc with the `allyesconfig` option

```
Usage: testgroup-llvm.sh [--help] [--kernel_dir value]

Arguments:
 --help: show this help
 --kernel_dir /home/user/linux: Path of the kernel directory

Environment Variables:
 --kernel_dir: KERNEL_DIR

```

### Bisect testgroup

It validates that a patchset can be bisectable. For every patch the following
tests are executed to evaluate its bisectability:

-   `test-build.sh`:
    -   Building allyesconfig ignoring warnings
-   `test-misc.sh`:
    -   Checks for unsafe strcpy and family

```
Usage: testgroup-bisect.sh [--help] [--kernel_dir value] [--git_origin value]

Arguments:
 --help: show this help
 --kernel_dir /home/user/linux: Path of the kernel directory
 --git_origin origin/main: GIT ref used for reference

Environment Variables:
 --kernel_dir: KERNEL_DIR
 --git_origin: GIT_ORIGIN
```

### Checkpatch testgroup

It validates that the code passes the `checkpatch.pl` check in strict mode, as
well as some custom media rules like:

-   Device tree code is not landed via our tree.
-   The top maintainers are not directly in cc.
-   Subject starts with "media:".

```
Usage: testgroup-checkpatch.sh [--help] [--kernel_dir value] [--git_origin value] [--patches_max value]

Arguments:
 --help: show this help
 --kernel_dir /home/user/linux: Path of the kernel directory
 --git_origin origin/main: GIT ref used for reference
 --patches_max 50: Max number of patches to be analyzed

Environment Variables:
 --kernel_dir: KERNEL_DIR
 --git_origin: GIT_ORIGIN
 --patches_max: PATCHES_MAX
```

### Doc testgroup

The documentation is formatted properly. It contains two tests:

-   `test-kernel-doc.sh`:
    -   Validates kernel-doc for all the media header files.
-   `test-spec.sh`:
    -   Creates a spec book with the media documentation.

```
Usage: testgroup-doc.sh [--help] [--kernel_dir value]

Arguments:
 --help: show this help
 --kernel_dir /home/user/linux: Path of the kernel directory

Environment Variables:
 --kernel_dir: KERNEL_DIR
```

### Static testgroup

Kernel static analyzers do not introduce new warnings.

-   `test-sparse.sh`:
    -   Use the sparse static analyzer building x86_64 in allyesconfig.
-   `test-smatch.sh`:
    -   Use the smatch static analyzer building x86_64 in allyesconfig.

```
Usage: testgroup-static.sh [--help] [--kernel_dir value]

Arguments:
 --help: show this help
 --kernel_dir /home/user/linux: Path of the kernel directory

Environment Variables:
 --kernel_dir: KERNEL_DIR
```

### Virtme testgroup

Runs the `test-media` script in a virtual machine using virtme. It runs the test
twice, in 64 bit and 32 bit compatibility mode.

```
Usage: testgroup-virtme.sh [--help] [--kernel_dir value]

Arguments:
 --help: show this help
 --kernel_dir /home/user/linux: Path of the kernel directory

Environment Variables:
 --kernel_dir: KERNEL_DIR
```

### Trust testgroup

Validates that at least two trusted reviewers has signed-off or reviewed each
patch. The list of trusted reviewers is at the `/uploaders.txt` file.
