#!/bin/sh
set -e

[ -z "$KERNEL_CONFIG" ] && exit 1

if [ -r "$KERNEL_CONFIG" ]; then
  cp "$KERNEL_CONFIG" .config
  make olddefconfig
  exit 0
fi

make "$KERNEL_CONFIG"

if [ "$KERNEL_CONFIG" = allyesconfig ]; then
  if [ "$ARCH" = x86 ];then
    scripts/config -d 64BIT
  fi
  scripts/config -d STAGING_EXCLUDE_BUILD -d KCOV -d WERROR
  perl -p -i -e 's/CONFIG_TOUCHSCREEN_(.*)=y/CONFIG_TOUCHSCREEN_\1=n/' .config
  scripts/config -e TOUCHSCREEN_SUR40
  make olddefconfig
fi
