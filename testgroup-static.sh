#!/bin/sh
set -e

CI_DIR=$(dirname "$(realpath "$0")")

VALID_ARGS="--kernel_dir"
. "$CI_DIR"/common-init.sh
cd "$KERNEL_DIR"

set -v
sh "$CI_DIR"/test-compile-all.sh
sh "$CI_DIR"/test-smatch.sh
sh "$CI_DIR"/test-sparse.sh
