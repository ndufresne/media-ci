#!/bin/sh
set -e

VALID_ARGS="--kernel_dir --cross_compile --virtme_script --virtme_args"

CI_DIR=$(dirname "$(realpath "$0")")
. "$CI_DIR"/common-init.sh

cd "$KERNEL_DIR" || exit 1

ARCH=x86_64
virtme-configkernel --defconfig --arch $ARCH
./scripts/config -e IKCONFIG -e IKCONFIG_PROC -e DEBUG_KMEMLEAK
./scripts/config -e DEBUG_KOBJECT -e DEBUG_OBJECTS_TIMERS -e DEBUG_KOBJECT_RELEASE
./scripts/config -e MEDIA_SUPPORT -e MEDIA_TEST_SUPPORT -e V4L_TEST_DRIVERS
./scripts/config -m VIDEO_VIMC -m VIDEO_VIVID -e VIDEO_VIVID_CEC
./scripts/config -m VIDEO_VIM2M -m VIDEO_VICODEC
./scripts/config -e MEDIA_DIGITAL_TV_SUPPORT -e DVB_TEST_DRIVERS
./scripts/config -e MEDIA_CONTROLLER_DVB -m DVB_VIDTV
KERNEL_CONFIG=olddefconfig sh "$CI_DIR"/kernel-config.sh
make all -j

virtme-run --kdir . --mods=auto --show-boot-console --verbose --cpus 2 --memory 4G --script-sh "$VIRTME_SCRIPT $VIRTME_ARGS" 2>&1 | tee virtme.log

grep "^Final Summary:" virtme.log || echo "Error running test" && exit 1
grep "^Final Summary:" virtme.log | grep -v ", Failed: 0," && echo "Errors Found!" && exit 1
grep "^Final Summary:" virtme.log | grep -v ", Warnings: 0$" && echo "Warnings Found!" && exit 1

. "$CI_DIR"/common-end.sh
